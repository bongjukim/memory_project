import { Mongo } from 'meteor/mongo';
 
export const Categorys = new Mongo.Collection('categorys');

// 인증된 사용자에 대한 데이터만 발행 하도록 설정
if (Meteor.isServer) {
	Meteor.publish('categorys', function() {		
		const selector = {
			// when logged in user is the owner
			$and: [{
				owner: this.userId
			}, {
				owner: {
					$exists: true
				}
			}]
		};
 
		return Categorys.find(selector);

		// 전체 발행
		// return Categorys.find();
	});
}
Categorys.allow({
	insert(userId, category) {
		return userId && category.owner === userId;
	},
	update(userId, category, fields, modifier) {
		return userId && category.owner === userId;
	},
	remove(userId, category) {
		return userId && category.owner === userId;
	}
});
