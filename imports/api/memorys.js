import { Mongo } from 'meteor/mongo';
 
export const Memorys = new Mongo.Collection('memorys');

// 인증된 사용자에 대한 데이터만 발행 하도록 설정
if (Meteor.isServer) {
	Meteor.publish('memorys', function() {
		const selector = {
			// when logged in user is the owner
			$and: [{
				owner: this.userId
			}, {
				owner: {
					$exists: true
				}
			}]
		};
 
		return Memorys.find(selector);

		// 전체 발행
		// return Memorys.find();
	});
}
Memorys.allow({
	insert(userId, memory) {
		return userId && memory.owner === userId;
	},
	update(userId, memory, fields, modifier) {
		return userId && memory.owner === userId;
	},
	remove(userId, memory) {
		return userId && memory.owner === userId;
	}
});
