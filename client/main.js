// setting
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
// account-setting
import '../imports/startup/accounts-config.js';
import { Meteor } from 'meteor/meteor';

// template
import indexUrl from '/imports/index.html';
import memoryListUrl from '/imports/memoryList.html';
import memoryFormUrl from '/imports/memoryForm.html';
import categoryFormUrl from '/imports/categoryForm.html';

// Mongo DB
import { Memorys } from '../imports/api/memorys.js';
import { Categorys } from '../imports/api/categorys.js';

angular.module('contactMgr', [angularMeteor, uiRouter, 'accounts.ui'])

.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state('index', {
            	url : "/",
            	views: {
            		'layout': {
            			templateUrl : indexUrl,
            			controller : 'indexCtrl'
            		}
            	}	
            })
            .state('list', {
                url: "/list?category",
                views: {
                    'layout': {
                        templateUrl: memoryListUrl,
                        controller : 'memoryListCtrl'
                    },
                }
            })
            .state('memoryForm', {
                url: "/memoryForm?mode&id",
                views: {
                    'layout': {
                        templateUrl: memoryFormUrl,
                        controller : 'memoryFormCtrl'
                    },
                }
            })
            .state('categoryForm', {
                url: "/categoryForm?mode&id",
                views: {
                    'layout': {
                        templateUrl: categoryFormUrl,
                        controller : 'categoryFormCtrl'
                    },
                }
            })
    }
])

// 모달 창 디렉티브
.directive('modal', function () {
    return {
        template: '<div class="modal fade">' + 
                    '<div class="modal-dialog">' + 
                        '<div class="modal-content">' + 
                            // '<div class="modal-header">' + 
                            //     '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                            //     '<h4 class="modal-title">{{ title }}</h4>' + 
                            // '</div>' + 
                        '<div class="modal-body" ng-transclude></div>' + 
                    '</div>' + 
                    '</div>' + 
                  '</div>',
        restrict: 'E',
        transclude: true,
        replace:true,
        scope:true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;

            scope.$watch(attrs.visible, function(value){
              if(value == true)
                $(element).modal('show');
              else
                $(element).modal('hide');
            });

            $(element).on('shown.bs.modal', function(){
              scope.$apply(function(){
                scope.$parent[attrs.visible] = true;
              });
            });

            $(element).on('hidden.bs.modal', function(){
              scope.$apply(function(){
                scope.$parent[attrs.visible] = false;
              });
            });
        }
    };
})

.controller('AppCtrl', function($scope, $meteor, $state, $rootScope, $stateParams) {
    // 서버에서 발행한 데이터를 구독
    $scope.subscribe('categorys');
    $scope.subscribe('memorys');

    // 로그인 여부 체크용 변수
    // $scope.isLogin = false;
    $scope.helpers({
        currentUser() {
            // 로그인을 했다면 유저id은 null이 아닌 값이 들어간다
            $scope.currentUserId = Meteor.userId();
            return Meteor.user();
        }
    })

    $scope.params = $stateParams;

    // 로그인 상태에 따른 state 이동 처리
    Tracker.autorun(function () {
      var userId = Meteor.userId();
      $scope.isLogin = false;
      // 로그인 상태가 아니라면 index 화면으로 이동
      if (!userId) {
        $state.go("index");  // go 'home' on logout
      // 로그인 상태라면 list 화면으로 이동
      } else if (userId) {
        $state.go("list");
        $scope.isLogin = true;
      }
    });

    // 로그인 하지 않으면 index 페이지 외엔 접근할 수 없도록 처리
    $rootScope.$on("$stateChangeSuccess", function() {
        var userId = Meteor.userId();
        if(!userId) {
            $state.go("index");
        }
    });
})

.controller('indexCtrl', function($scope, $state, $rootScope, $stateParams) {
	// $rootScope.$on("$stateChangeSuccess", function() {
 //    	console.log(Meteor.userId());
 //    	if(Meteor.userId()) {
 //    		$state.go("list");
 //    	}
 //    })
})

.controller('memoryListCtrl', function($scope, $state, $rootScope, $stateParams) {

    // $scope.showModal = false;

    $scope.openCategoryMenu = function(){
        $('#mask')
            .fadeTo("fast",0.5)
            // .css('height', $('#memory-area').height());
            .css('height', '9999999999px');
        $('#category-toggle-area').css('height', '150px');
    }

    $scope.closeCategoryMenu = function($event){
        // 버블링 방지코드
        $event.stopPropagation();
        $event.preventDefault();
        $('#mask').hide(0.5);
        $('#category-toggle-area').css('height', '15px');
    }

    $scope.showMemoryExplanation = function() {
        $(".figure").addClass("memory-explanation");
    }

    $scope.helpers({
        categorys() {
            var categorys = [];
            // 전체 추억상자는 id를 0으로 지정
            categorys.push({_id: "0", name: "ALL", explanation: "전체", memoryCnt: Memorys.find().count()})
            var allCategorys = Categorys.find({}).fetch();
            // console.log(allCategorys);
            allCategorys.forEach(function(category) {
                // console.log(category);
                memoryCnt = Memorys.find({category: category._id}).count();
                category.memoryCnt = memoryCnt;
                categorys.push(category);
            })
            return categorys
        }
    });

    $scope.helpers({
        memorys() {
            var memorys = [];
            if($stateParams.category == undefined) {
                var allMemorys = Memorys.find({}).fetch();
            } else {
                var allMemorys = Memorys.find({category:$stateParams.category}).fetch();    
            }
            // console.log(allMemorys);
            allMemorys.forEach(function(memory) {
                // console.log(memory)
                if(memory.category != 0) {
                    var category = Categorys.findOne({_id:memory.category});
                    // 카테고리 id에 맞는 name 찾아주기
                    memory.categoryName = category.name;
                }
                memorys.push(memory);
            })
            return memorys
        }
    });

    $scope.filterMemorys = function(categoryId) {
        // alert(categoryId);
        if(categoryId === "0") {
            $state.go("list",{},{reload:true, inherit:false});
        } else {
            $state.go("list",{category:categoryId},{reload:true});
        }
    }

    $scope.detailMemory = function(index) {
        // console.log(index)
        $scope.memoryIndex = index;
        $scope.showModal = true;
        $scope.memoryDetail = $scope.memorys[index]
    }

    $scope.previousMemory = function() {
        $scope.showModal = false;
        $scope.detailMemory($scope.memoryIndex - 1);
    }

    $scope.nextMemory = function() {
        $scope.showModal = false;
        $scope.detailMemory($scope.memoryIndex + 1);
    }

    // 추억 삭제어
    $scope.deleteMemory = function(id) {
        if(confirm("정말 삭제하시겠습니까??") === true) {
            Memorys.remove({_id: id});
        }

        $state.go("list");
    }

    // 추억상자 삭제
    $scope.deleteCategory = function(id) {
        var rmMemorys = Memorys.find({category: id});
        var categoryCnt = Memorys.find({category: id}).count();
        if (categoryCnt > 0) {
            if(confirm("해당 추억상자에 "+categoryCnt+"개의 추억이 존재합니다. \n추억상자 삭제시 추억도 함께 삭제됩니다. 정말 삭제하시겠습니까??") == true) {
                rmMemorys.forEach(function(category) {
                    Memorys.remove({_id: category._id});
                })
                Categorys.remove({_id: id});
            } else {

            }
        } else {
            if(confirm("정말 삭제하시겠습니까??") == true) {
                Categorys.remove({_id: id});

            } else {

            }
        }
    };
})
.controller('memoryFormCtrl', function($scope, $state, $rootScope, $stateParams) {
    $scope.mode = $stateParams.mode;
    $scope.memoryId = $stateParams.id;

    $(".memory-modal").modal('hide');

    // 추억 폼 데이터
    $scope.memoryForm = {id: $scope.memoryId, category: '', url: '', explanation: ''};

    // 추억상자 리스트 가져오기
    $scope.helpers({
        categorys() {
            var categorys = [];
            // 전체 추억상자는 id를 0으로 지정
            categorys.push({_id: "0", name: "추억상자없음", explanation: "추억상자없음"})
            var allCategorys = Categorys.find({}).fetch();
            // console.log(allCategorys);
            allCategorys.forEach(function(category) {
                // console.log(category)
                categorys.push(category);
            })
            return categorys
        }
    });

    // 기본값 설정
    $scope.memoryForm.category = "0"

    // 수정일 때
    if($scope.memoryId != undefined) {
        var memory = Memorys.findOne({_id: $scope.memoryId});
        $scope.memoryForm.category = memory.category;
        $scope.memoryForm.url = memory.url;
        $scope.memoryForm.explanation = memory.explanation;
    }

    // 추억 폼 유효성 검사
    $scope.validation = function() {
        if($scope.memoryForm.category === undefined || $scope.memoryForm.category.trim().length === 0) {
            alert("추억상자를 선택하세요.");
        } else if($scope.memoryForm.url === undefined || $scope.memoryForm.url.trim().length === 0) {
            alert("추억 url을 입력하세요.");
        } else if($scope.memoryForm.explanation === undefined || $scope.memoryForm.explanation.trim().length === 0) {
            alert("추억 메모를 입력하세요.");
        } else {
            return true;
        }
    };

    // 추억 추가
    $scope.addMemory = function() {
        if($scope.validation()) {
            Memorys.insert({
                category: $scope.memoryForm.category,
                url: $scope.memoryForm.url,
                explanation: $scope.memoryForm.explanation,
                createdAt: new Date(),
                owner: Meteor.userId(),
                username: Meteor.user().username
            });
            $state.go("list");
        } else {
            // alert("추억 등록에 실패하였습니다.");
        }
    };

    // 추억 수정
    $scope.modifyMemory = function() {
        if($scope.validation()) {
            Memorys.update($scope.memoryId, {
                $set: {
                    category: $scope.memoryForm.category,
                    url: $scope.memoryForm.url,
                    explanation: $scope.memoryForm.explanation,
                    owner: Meteor.userId(),
                    username: Meteor.user().username
                }
            });
            $state.go("list");
        } else {
            // alert("추억 등록에 실패하였습니다.");
        }
    }
})
.controller('categoryFormCtrl', function($scope, $state, $rootScope, $stateParams) {
    $scope.mode = $stateParams.mode;
    $scope.categoryId = $stateParams.id;

    // 추억상자 폼 데이터
    $scope.categoryForm = {id: $scope.categoryId, name: '', explanation: ''};

    // 수정일 때
    if($scope.categoryId != undefined) {
        var category = Categorys.findOne({_id: $scope.categoryId});
        $scope.categoryForm.name = category.name;
        $scope.categoryForm.explanation = category.explanation;
    }

    // 추억상자 폼 유효성 검사
    $scope.validation = function() {
        if($scope.categoryForm.name === undefined || $scope.categoryForm.name.trim().length === 0) {
            alert("추억상자 이름을 입력하세요");
        } else if($scope.categoryForm.explanation === undefined || $scope.categoryForm.explanation.trim().length === 0) {
            alert("추억상자 설명을 입력하세요");
        } else {
            return true;
        }
    };

    // 추억상자 추가
    $scope.addCategory = function() {
        if($scope.validation()) {
            // console.log($scope.categoryForm);
            Categorys.insert({
                name: $scope.categoryForm.name,
                explanation: $scope.categoryForm.explanation,
                createdAt: new Date(),
                owner: Meteor.userId(),
                username: Meteor.user().username
            });
            $state.go("list");
        } else {
            // alert("추억상자 등록에 실패하였습니다.");
        }
    };

    // 추억상자 수정
    $scope.modifyCategory = function() {
        if($scope.validation()) {
            Categorys.update($scope.categoryId, {
                $set: {
                    name: $scope.categoryForm.name,
                    explanation: $scope.categoryForm.explanation,
                    owner: Meteor.userId(),
                    username: Meteor.user().username
                }
            });
            $state.go("list");
        } else {
            // alert("추억상자 등록에 실패하였습니다.");
        }
    }

    // console.log($stateParams);
})